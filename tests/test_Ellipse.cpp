// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <trajseg-cpp/Ellipse.h>

#include <gtest/gtest.h>

using namespace trajseg;
using namespace advcpp;
using namespace in4D;

using ::testing::InitGoogleTest;

TEST(EllipseTest, EllipseLen)
{
    Point start(0.0, 3.0, 0.0);
    Point end(10.0, 3.0, 0.0);
    Point center(5.0, 3.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);
    Vector majorDir(1.0, 0.0, 0.0);

    Ellipse e(start, end, center, normal, majorDir, 5.0, 3.0, 0.01);
    ASSERT_NEAR(e.len(), 12.763499431699065, 0.00001);
}

TEST(EllipseTest, EllipseParameterAngle)
{
    Point start(0.0, 3.0, 0.0);
    Point end(10.0, 3.0, 0.0);
    Point center(5.0, 3.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);
    Vector majorDir(1.0, 0.0, 0.0);

    Ellipse e(start, end, center, normal, majorDir, 5.0, 3.0, 0.0001);

    auto angle = e.parameterAngle(0);
    ASSERT_NEAR(angle, M_PI, 0.0001);

    angle = e.parameterAngle(12.763499431699065 / 2);
    ASSERT_NEAR(angle, M_PI + M_PI / 2, 0.0001);

    angle = e.parameterAngle(12.763499431699065);
    ASSERT_NEAR(angle, 2 * M_PI, 0.0001);
}

TEST(EllipseTest, EllipseAt)
{
    Point start(0.0, 3.0, 0.0);
    Point end(10.0, 3.0, 0.0);
    Point center(5.0, 3.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);
    Vector majorDir(1.0, 0.0, 0.0);

    Ellipse e(start, end, center, normal, majorDir, 5.0, 3.0, 0.0001);

    auto p = e.at(0.0);
    ASSERT_NEAR(p.x, 0.0, 0.0001);
    ASSERT_NEAR(p.y, 3.0, 0.0001);
    ASSERT_NEAR(p.z, 0.0, 0.0001);

    p = e.at(12.763499431699065 / 2);
    ASSERT_NEAR(p.x, 5.0, 0.0001);
    ASSERT_NEAR(p.y, 0.0, 0.0001);
    ASSERT_NEAR(p.z, 0.0, 0.0001);

    p = e.at(12.763499431699065);
    ASSERT_NEAR(p.x, 10.0, 0.0001);
    ASSERT_NEAR(p.y, 3.0, 0.0001);
    ASSERT_NEAR(p.z, 0.0, 0.0001);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
