// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <trajseg-cpp/Ellipse.h>
#include <trajseg-cpp/Helix.h>
#include <trajseg-cpp/Trajectory.h>

#include <gtest/gtest.h>

using namespace trajseg;
using namespace advcpp;
using namespace in4D;

using ::testing::InitGoogleTest;

TEST(TrajectoryTest, TrajectorySegmentAt)
{
    Point start1(0.0, 3.0, 0.0);
    Point end1(10.0, 3.0, 0.0);
    Point center1(5.0, 3.0, 0.0);
    Vector normal1(0.0, 0.0, 1.0);
    Vector majorDir1(1.0, 0.0, 0.0);

    Segment<double> *seg1Ptr =
        new Ellipse<double>(start1, end1, center1, normal1, majorDir1, 5.0, 3.0, 0.0001);

    Point center2(end1.x + 4.0, end1.y + 4.0, end1.z + 0.0);
    Vector normal2(0.0, 0.0, 1.0);

    Segment<double> *seg2Ptr = new Helix(end1, center2, normal2, 1.0, 5.0);

    Trajectory t;
    t.pushBack(seg1Ptr);
    t.pushBack(seg2Ptr);

    ASSERT_NEAR(t.len(), seg1Ptr->len() + seg2Ptr->len(), 0.0001);

    auto [segPtr1, pos1] = t.segmentAt(12.763499431699065 / 2);
    ASSERT_EQ(segPtr1->segmentClassId(), Ellipse<double>::ClassId);
    ASSERT_EQ(pos1, 0.0);

    auto [segPtr2, pos2] = t.segmentAt(seg1Ptr->len() + seg2Ptr->len());
    ASSERT_EQ(segPtr2->segmentClassId(), Helix<double>::ClassId);
    ASSERT_EQ(pos2, seg1Ptr->len());
}

TEST(TrajectoryTest, TrajectoryAt)
{
    Point start1(0.0, 3.0, 0.0);
    Point end1(10.0, 3.0, 0.0);
    Point center1(5.0, 3.0, 0.0);
    Vector normal1(0.0, 0.0, 1.0);
    Vector majorDir1(1.0, 0.0, 0.0);

    Segment<double> *seg1Ptr =
        new Ellipse<double>(start1, end1, center1, normal1, majorDir1, 5.0, 3.0, 0.0001);

    Point center2(end1.x + 4.0, end1.y + 4.0, end1.z + 0.0);
    Vector normal2(0.0, 0.0, 1.0);

    Segment<double> *seg2Ptr = new Helix(end1, center2, normal2, 1.0, 5.0);

    Trajectory t;
    t.pushBack(seg1Ptr);
    t.pushBack(seg2Ptr);

    Point p = t.at(12.763499431699065 / 2);
    ASSERT_NEAR(p.x, 5.0, 0.0001);
    ASSERT_NEAR(p.y, 0.0, 0.0001);
    ASSERT_NEAR(p.z, 0.0, 0.0001);

    p = t.at(seg1Ptr->len() + seg2Ptr->len());
    ASSERT_NEAR(p.x, end1.x, 0.00001);
    ASSERT_NEAR(p.y, end1.y, 0.00001);
    ASSERT_EQ(p.z, end1.z + 5.0);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
