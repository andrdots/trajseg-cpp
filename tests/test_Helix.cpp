// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <trajseg-cpp/Helix.h>

#include <gtest/gtest.h>

using namespace trajseg;
using namespace advcpp;
using namespace in4D;

using ::testing::InitGoogleTest;

TEST(HelixTest, HelixEndAndLen)
{
    Point start(1.0, 1.0, 0.0);
    Point center(5.0, 5.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);

    Helix h(start, center, normal, 1.0, 5.0);

    auto end = h.end();
    ASSERT_NEAR(end.x, start.x, 0.00001);
    ASSERT_NEAR(end.y, start.y, 0.00001);
    ASSERT_EQ(end.z, 5.0);

    auto len = sqrt((pow((5 / 1) * 2 * M_PI, 2) * (4 * 4 + 4 * 4)) + 5 * 5);
    ASSERT_NEAR(len, 177.7856408248, 0.00001);
    ASSERT_NEAR(h.len(), len, 0.00001);

    start.x = 10.0;
    start.y = 3.0;
    h = Helix(start, center, normal, 1.0, 5.0);

    end = h.end();
    ASSERT_NEAR(end.x, start.x, 0.00001);
    ASSERT_NEAR(end.y, start.y, 0.00001);
    ASSERT_EQ(end.z, 5.0);
}

TEST(HelixTest, HelixAt)
{
    Point start(1.0, 1.0, 0.0);
    Point center(5.0, 5.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);

    Helix h(start, center, normal, 1.0, 5.0);

    auto p = h.at(0);
    ASSERT_NEAR(p.x, 1.0, 0.00001);
    ASSERT_NEAR(p.y, 1.0, 0.00001);
    ASSERT_EQ(p.z, 0.0);

    auto len = h.len();
    p = h.at(len);
    ASSERT_NEAR(p.x, 1.0, 0.00001);
    ASSERT_NEAR(p.y, 1.0, 0.00001);
    ASSERT_NEAR(p.z, 5.0, 0.00001);

    p = h.at(len / 2);
    ASSERT_NEAR(p.x, 9.0, 0.00001);
    ASSERT_NEAR(p.y, 9.0, 0.00001);
    ASSERT_EQ(p.z, 2.5);
}

TEST(HelixTest, HelixTangent)
{
    Point start(1.0, 1.0, 0.0);
    Point center(5.0, 5.0, 0.0);
    Vector normal(0.0, 0.0, 1.0);

    Helix h(start, center, normal, 1.0, 5.0);

    Vector t(cos(M_PI / 4), -sin(M_PI / 4), 5 / h.len());
    t.normalize();

    auto p = h.tangent(0);
    ASSERT_NEAR(p.x, t.x, 0.00001);
    ASSERT_NEAR(p.y, t.y, 0.00001);
    ASSERT_NEAR(p.z, t.z, 0.0001);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
