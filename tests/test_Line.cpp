// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <trajseg-cpp/Line.h>

#include <gtest/gtest.h>

using namespace trajseg;
using namespace advcpp;
using namespace in4D;

using ::testing::InitGoogleTest;

TEST(LineTest, AtPlus)
{
    Point start1(10.0, 5.0, 2.0);
    Point end1(20.0, 7.0, 8.0);

    Line line(start1, end1);

    ASSERT_NEAR(line.len(), 11.832159566199, 0.0001);

    Point p = line.at(0);
    ASSERT_EQ(p.x, start1.x);
    ASSERT_EQ(p.y, start1.y);
    ASSERT_EQ(p.z, start1.z);

    p = line.at(line.len() / 2);
    ASSERT_NEAR(p.x, (start1.x + end1.x) / 2, 0.0001);
    ASSERT_NEAR(p.y, (start1.y + end1.y) / 2, 0.0001);
    ASSERT_NEAR(p.z, (start1.z + end1.z) / 2, 0.0001);

    p = line.at(line.len());
    ASSERT_EQ(p.x, end1.x);
    ASSERT_EQ(p.y, end1.y);
    ASSERT_EQ(p.z, end1.z);
}

TEST(LineTest, AtMinus)
{
    Point start1(-10.0, -5.0, -2.0);
    Point end1(-20.0, -7.0, -8.0);

    Line line(start1, end1);

    ASSERT_NEAR(line.len(), 11.832159566199, 0.0001);

    Point p = line.at(0);
    ASSERT_EQ(p.x, start1.x);
    ASSERT_EQ(p.y, start1.y);
    ASSERT_EQ(p.z, start1.z);

    p = line.at(line.len() / 2);
    ASSERT_NEAR(p.x, (start1.x + end1.x) / 2, 0.0001);
    ASSERT_NEAR(p.y, (start1.y + end1.y) / 2, 0.0001);
    ASSERT_NEAR(p.z, (start1.z + end1.z) / 2, 0.0001);

    p = line.at(line.len());
    ASSERT_EQ(p.x, end1.x);
    ASSERT_EQ(p.y, end1.y);
    ASSERT_EQ(p.z, end1.z);
}

TEST(LineTest, Tangent)
{
    Point start1(10.0, 5.0, 2.0);
    Point end1(20.0, 7.0, 8.0);

    Line line(start1, end1);

    Vector tangent = line.tangent(0.0);
    ASSERT_NEAR(tangent.x, 0.8451542547285166, 0.0001);
    ASSERT_NEAR(tangent.y, 0.1690308509457033, 0.0001);
    ASSERT_NEAR(tangent.z, 0.50709255283711, 0.0001);

    Vector t = line.tangent(line.len());
    ASSERT_EQ(t.x, tangent.x);
    ASSERT_EQ(t.y, tangent.y);
    ASSERT_EQ(t.z, tangent.z);

    start1 = Point(-10.0, -5.0, -2.0);
    end1 = Point(-20.0, -7.0, -8.0);

    line = Line(start1, end1);

    tangent = line.tangent(0.0);
    ASSERT_NEAR(tangent.x, -0.8451542547285166, 0.0001);
    ASSERT_NEAR(tangent.y, -0.1690308509457033, 0.0001);
    ASSERT_NEAR(tangent.z, -0.50709255283711, 0.0001);

    t = line.tangent(line.len());
    ASSERT_EQ(t.x, tangent.x);
    ASSERT_EQ(t.y, tangent.y);
    ASSERT_EQ(t.z, tangent.z);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
