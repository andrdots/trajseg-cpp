// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef TRAJSEG_CPP_Segment_H_
#define TRAJSEG_CPP_Segment_H_

#include <advcpp/Vector.h>

#include <string>

namespace trajseg
{

template<typename T = double>
class Segment
{
public:
    Segment(const advcpp::in4D::Point<T> &start, const advcpp::in4D::Point<T> &end);
    virtual ~Segment() = default;

    inline const advcpp::in4D::Point<T> &start();
    inline const advcpp::in4D::Point<T> &end();

    virtual T len() { return m_len; }
    virtual advcpp::in4D::Point<T> at(T pos) = 0;
    virtual advcpp::in4D::Vector<T> tangent(T pos) = 0;

    virtual std::string className() const { return "Segment"; }
    virtual unsigned int segmentClassId() const = 0;

protected:
    Segment() = default;

    advcpp::in4D::Point<T> m_start;
    advcpp::in4D::Point<T> m_end;
    T m_len = 0;
};

template<typename T>
Segment<T>::Segment(const advcpp::in4D::Point<T> &start, const advcpp::in4D::Point<T> &end)
    : m_start(start), m_end(end)
{
}

template<typename T>
const advcpp::in4D::Point<T> &
Segment<T>::start()
{
    return m_start;
}

template<typename T>
const advcpp::in4D::Point<T> &
Segment<T>::end()
{
    return m_end;
}

} // namespace trajseg

#endif /* TRAJSEG_CPP_Segment_H_ */
