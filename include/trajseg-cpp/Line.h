// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef TRAJSEG_CPP_Line_H_
#define TRAJSEG_CPP_Line_H_

#include "Segment.h"

#include <advcpp/Matrix.h>
#include <advcpp/circle.h>

namespace trajseg
{

template<typename T>
class Line : public Segment<T>
{
public:
    enum { ClassId = 1 };

    explicit Line(const advcpp::in4D::Point<T> &start, const advcpp::in4D::Point<T> &end);

    advcpp::in4D::Point<T> at(T pos) override;
    advcpp::in4D::Vector<T> tangent(T pos) override;

    std::string className() const override { return "Line"; }
    unsigned int segmentClassId() const override { return ClassId; }

protected:
    Line() {}

    advcpp::in4D::Vector<T> m_dir;
};

template<typename T>
Line<T>::Line(const advcpp::in4D::Point<T> &start, const advcpp::in4D::Point<T> &end)
    : Segment<T>(start, end)
{
    advcpp::in4D::Vector<T> v = end - start;
    this->m_len = v.len();
    m_dir = v.normalized();
}

template<typename T>
advcpp::in4D::Point<T>
Line<T>::at(T pos)
{
    T k = pos / this->m_len;
    return this->m_start + (this->m_end - this->m_start) * k;
}

template<typename T>
advcpp::in4D::Vector<T>
Line<T>::tangent(T /* unused */)
{
    return m_dir;
}

} // namespace trajseg

#endif /* TRAJSEG_CPP_Line_H_ */
