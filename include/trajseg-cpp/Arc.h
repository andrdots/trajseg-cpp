// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef TRAJSEG_CPP_Arc_H_
#define TRAJSEG_CPP_Arc_H_

#include "Segment.h"

#include <advcpp/Matrix.h>
#include <advcpp/circle.h>

namespace trajseg
{

template<typename T>
class Arc : public Segment<T>
{
public:
    enum { ClassId = 2 };

    explicit Arc(
        const advcpp::in4D::Point<T> &start,
        const advcpp::in4D::Point<T> &middle,
        const advcpp::in4D::Point<T> &end);

    advcpp::in4D::Point<T> at(T pos) override;
    advcpp::in4D::Vector<T> tangent(T pos) override;

    inline T radius() const { return m_radius; }
    inline const advcpp::in4D::Point<T> &center() const { return m_center; }
    inline const advcpp::in4D::Vector<T> &normal() const { return m_normal; }

    std::string className() const override { return "Arc"; }
    unsigned int segmentClassId() const override { return ClassId; }

protected:
    Arc() {}

    advcpp::in4D::Point<T> m_center;
    advcpp::in4D::Vector<T> m_normal;
    T m_radius;
    T m_angle;
};

template<typename T>
Arc<T>::Arc(
    const advcpp::in4D::Point<T> &start,
    const advcpp::in4D::Point<T> &middle,
    const advcpp::in4D::Point<T> &end)
    : Segment<T>(start, end)
{
    m_center = advcpp::circle::GetCenter(start, middle, end);
    m_normal = (middle - start) * (end - middle);
    m_angle = advcpp::circle::GetArcAngle(this->m_start, this->m_end, m_center, m_normal);
    m_radius = (start - m_center).len();
    this->m_len = m_angle * m_radius;
}

template<typename T>
advcpp::in4D::Point<T>
Arc<T>::at(T pos)
{
    T angle = pos / m_radius;
    return advcpp::circle::GetArcPointByAngle(this->m_start, this->m_center, m_normal, angle);
}

template<typename T>
advcpp::in4D::Vector<T>
Arc<T>::tangent(T pos)
{
    T angle = pos / m_radius;
    return advcpp::circle::GetArcTangentByAngle(this->m_start, this->m_center, m_normal, angle);
}

} // namespace trajseg

#endif /* TRAJSEG_CPP_Arc_H_ */
