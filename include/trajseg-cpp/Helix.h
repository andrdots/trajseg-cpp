#ifndef TRAJSEG_CPP_Helix_H_
#define TRAJSEG_CPP_Helix_H_

#include "Segment.h"

#include <advcpp/Matrix.h>
#include <advcpp/circle.h>

namespace trajseg
{

template<typename T>
class Helix : public Segment<T>
{
public:
    enum { ClassId = 3 };

    explicit Helix(
        const advcpp::in4D::Point<T> &start,
        const advcpp::in4D::Point<T> &center,
        const advcpp::in4D::Vector<T> &normal,
        T step,
        T height);

    advcpp::in4D::Point<T> at(T pos) override;
    advcpp::in4D::Vector<T> tangent(T pos) override;

    inline T radius() const { return m_radius; }
    inline const advcpp::in4D::Point<T> &center() const { return m_center; }
    inline const advcpp::in4D::Vector<T> &normal() const { return m_normal; }

    inline T height() const { return m_height; }
    inline T step() const { return m_step; }

    std::string className() const override { return "Helix"; }
    unsigned int segmentClassId() const override { return ClassId; }

protected:
    advcpp::in4D::Point<T> m_center;
    advcpp::in4D::Vector<T> m_normal;
    T m_radius;
    T m_angle;
    T m_height;
    T m_step;
    T m_angleUp;
};

template<typename T>
Helix<T>::Helix(
    const advcpp::in4D::Point<T> &start,
    const advcpp::in4D::Point<T> &center,
    const advcpp::in4D::Vector<T> &normal,
    T step,
    T height)
    : m_center(center), m_normal(normal), m_height(height), m_step(step)
{
    this->m_start = start;
    this->m_radius = (start - this->m_center).len();
    this->m_angle = 2 * M_PI * height / step;
    this->m_end = advcpp::circle::GetArcPointByAngle(start, center, normal, this->m_angle);
    this->m_end = this->m_end + normal * height;

    T projLen = this->m_angle * this->m_radius;
    this->m_len = sqrt(projLen * projLen + m_height * m_height);

    m_angleUp = atan2(m_height, projLen);
}

template<typename T>
advcpp::in4D::Point<T>
Helix<T>::at(T pos)
{
    T k = pos / this->m_len;
    T angle = k * this->m_angle;
    advcpp::in4D::Point<T> p =
        advcpp::circle::GetArcPointByAngle(this->m_start, this->m_center, this->m_normal, angle);
    p.z += k * this->m_height;
    return p;
}

template<typename T>
advcpp::in4D::Vector<T>
Helix<T>::tangent(T pos)
{
    T k = pos / this->m_len;
    T angle = k * this->m_angle;
    advcpp::in4D::Vector<T> tangent = advcpp::circle::GetArcTangentByAngle(
        this->m_start,
        this->m_center,
        this->m_normal,
        angle,
        m_angleUp);
    return tangent;
}

} // namespace trajseg

#endif /* TRAJSEG_CPP_Helix_H_ */
