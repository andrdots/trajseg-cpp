// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef TRAJSEG_CPP_Trajectory_H_
#define TRAJSEG_CPP_Trajectory_H_

#include <advcpp/Vector.h>
#include <trajseg-cpp/Segment.h>

#include <algorithm>
#include <list>
#include <map>
#include <memory>

namespace trajseg
{

template<typename T = double>
class Trajectory : public Segment<T>
{
public:
    using SegmentPtr = std::shared_ptr<Segment<T>>;
    enum { ClassId = 0 };

    using reference = Segment<T> &;
    using const_reference = Segment<T> const &;

    virtual ~Trajectory() = default;

    reference pushBack(Segment<T> *seg);

    template<typename... Args>
    reference emplaceBack(Args &&...args);

    std::pair<SegmentPtr, T> segmentAt(T pos);

    advcpp::in4D::Point<T> at(T pos) override;
    advcpp::in4D::Vector<T> tangent(T pos) override;

    std::string className() const override { return "Trajectory"; }
    unsigned int segmentClassId() const override { return ClassId; }

    using const_iterator = typename std::list<SegmentPtr>::const_iterator;

    const_iterator cbegin();
    const_iterator cend();
    const_iterator begin() const;
    const_iterator end() const;

protected:
    std::list<SegmentPtr> m_segments;
    std::map<T, typename std::list<SegmentPtr>::iterator, std::greater<T>> m_byStartPos;
};

template<typename T>
typename Trajectory<T>::reference
Trajectory<T>::pushBack(Segment<T> *segPtr)
{
    auto segPtr_tmp = m_segments.emplace_back(segPtr);

    auto lastSegIter = std::prev(m_segments.end());
    m_byStartPos.emplace(this->m_len, lastSegIter);

    this->m_len += segPtr->len();
    return *segPtr_tmp;
}

template<typename T>
template<typename... Args>
typename Trajectory<T>::reference
Trajectory<T>::emplaceBack(Args &&...args)
{
    auto segPtr = m_segments.emplace_back(std::forward<Args>(args)...);

    auto lastSegIter = std::prev(m_segments.end());
    m_byStartPos.emplace(this->m_len, lastSegIter);

    this->m_len += segPtr->len();
    return *segPtr;
}

template<typename T>
std::pair<typename Trajectory<T>::SegmentPtr, T>
Trajectory<T>::segmentAt(T pos)
{
    auto lowerIter = m_byStartPos.lower_bound(pos);
    SegmentPtr segPtr;
    T startPos;
    if (lowerIter == m_byStartPos.end()) {
        segPtr = *m_segments.begin();
        startPos = 0;
    } else {
        segPtr = *lowerIter->second;
        startPos = lowerIter->first;
    }
    return {segPtr, startPos};
}

template<typename T>
advcpp::in4D::Point<T>
Trajectory<T>::at(T pos)
{
    auto [segPtr, startPos] = segmentAt(pos);
    return segPtr->at(pos - startPos);
}

template<typename T>
advcpp::in4D::Vector<T>
Trajectory<T>::tangent(T pos)
{
    auto [segPtr, startPos] = segmentAt(pos);
    return segPtr->tangent(pos - startPos);
}

template<typename T>
typename Trajectory<T>::const_iterator
Trajectory<T>::cbegin()
{
    return m_segments.cbegin();
}

template<typename T>
typename Trajectory<T>::const_iterator
Trajectory<T>::cend()
{
    return m_segments.cend();
}

template<typename T>
typename Trajectory<T>::const_iterator
Trajectory<T>::begin() const
{
    return m_segments.cbegin();
}

template<typename T>
typename Trajectory<T>::const_iterator
Trajectory<T>::end() const
{
    return m_segments.cend();
}

} // namespace trajseg

#endif /* TRAJSEG_CPP_Trajectory_H_ */
