#ifndef TRAJSEG_CPP_Ellipse_H_
#define TRAJSEG_CPP_Ellipse_H_

#include "Segment.h"

#include <advcpp/Matrix.h>
#include <advcpp/ellipse.h>

#include <tuple>
#include <vector>

namespace trajseg
{

template<typename T>
class Ellipse : public Segment<T>
{
public:
    enum { ClassId = 4 };

    explicit Ellipse(
        const advcpp::in4D::Point<T> &start,
        const advcpp::in4D::Point<T> &end,
        const advcpp::in4D::Point<T> &center,
        const advcpp::in4D::Vector<T> &normal,
        const advcpp::in4D::Vector<T> &majorDir,
        T majorRadius,
        T minorRadius,
        T precision);

    advcpp::in4D::Point<T> at(T pos) override;
    advcpp::in4D::Vector<T> tangent(T pos) override;

    T parameterAngle(T pos);

    inline T majorRadius() const { return m_majorRadius; }
    inline T minorRadius() const { return m_minorRadius; }
    inline const advcpp::in4D::Point<T> &center() const { return m_center; }
    inline const advcpp::in4D::Vector<T> &normal() const { return m_normal; }

    std::string className() const override { return "Ellipse"; }
    unsigned int segmentClassId() const override { return ClassId; }

protected:
    advcpp::in4D::Point<T> m_center;
    advcpp::in4D::Vector<T> m_normal;
    advcpp::in4D::Vector<T> m_majorDir;
    advcpp::in4D::Vector<T> m_minorDir;
    T m_majorRadius;
    T m_minorRadius;
    T m_startAngle;
    T m_endAngle;
    T m_precision;

private:
    std::vector<T> m_angleByLen;

    std::vector<T> computeAnglesByLen();
};

template<typename T>
Ellipse<T>::Ellipse(
    const advcpp::in4D::Point<T> &start,
    const advcpp::in4D::Point<T> &end,
    const advcpp::in4D::Point<T> &center,
    const advcpp::in4D::Vector<T> &normal,
    const advcpp::in4D::Vector<T> &majorDir,
    T majorRadius,
    T minorRadius,
    T precision)
    : m_center(center), m_normal(normal), m_majorDir(majorDir), m_majorRadius(majorRadius),
      m_minorRadius(minorRadius), m_precision(precision)
{
    this->m_start = start;
    this->m_end = end;

    m_minorDir = m_normal * m_majorDir;

    m_startAngle = advcpp::ellipse::GetParameterAngleByPoint(
        start,
        center,
        normal,
        majorDir,
        majorRadius,
        minorRadius);
    m_endAngle = advcpp::ellipse::GetParameterAngleByPoint(
        end,
        center,
        normal,
        majorDir,
        majorRadius,
        minorRadius);
    if (m_endAngle < m_startAngle) {
        m_endAngle += 2 * M_PI;
    }
    this->m_len =
        advcpp::ellipse::ComputeArcLength(majorRadius, minorRadius, m_startAngle, m_endAngle);

    m_angleByLen = computeAnglesByLen();
}

template<typename T>
std::vector<T>
Ellipse<T>::computeAnglesByLen()
{
    if (this->m_len <= std::numeric_limits<T>::epsilon()) {
        return {};
    }

    int count = this->m_len / m_precision;
    if (count < 2) {
        count = 2;
    }
    std::vector<T> angles(count);
    angles[0] = m_startAngle;
    T dAngle = ((m_endAngle - m_startAngle) * ((m_majorRadius + m_minorRadius) / 2)) / count;
    T angle = m_startAngle;
    T len = 0;
    for (int i = 1; i < count; ++i) {
        T expectedLen = (this->m_len / count) * i;
        T expectedDLen = expectedLen - len;
        T dLen;
        std::tie(dAngle, dLen) = advcpp::ellipse::ComputeApproxParameterAngleByLen(
            m_majorRadius,
            m_minorRadius,
            angle,
            expectedDLen,
            expectedDLen / 4,
            dAngle);
        len += dLen;
        angle += dAngle;
        angles[i] = angle;
    }
    return angles;
}

template<typename T>
T
Ellipse<T>::parameterAngle(T pos)
{
    if ((m_angleByLen.size() == 0) || (pos == 0)) {
        return m_startAngle;
    }
    if (pos == this->m_len) {
        return m_endAngle;
    }
    if (pos > this->m_len) {
        T dAngle;
        std::tie(dAngle, std::ignore) = advcpp::ellipse::ComputeApproxParameterAngleByLen(
            m_majorRadius,
            m_minorRadius,
            m_endAngle,
            pos - this->m_len,
            m_precision / 4);
        return m_endAngle + dAngle;
    }

    T k = (pos / this->m_len);
    if (k < 0) {
        k = 0;
    }

    size_t n = k * (m_angleByLen.size() - 1);
    T angle = m_angleByLen[n];

    T neighborAngle;
    if (n < m_angleByLen.size() - 1) {
        neighborAngle = (m_angleByLen[n + 1] - m_angleByLen[n]);
    } else {
        neighborAngle = (m_angleByLen[n] - m_angleByLen[n - 1]);
    }
    T rest = k * (m_angleByLen.size() - 1) - n;
    angle += rest * neighborAngle;
    return angle;
}

template<typename T>
advcpp::in4D::Point<T>
Ellipse<T>::at(T pos)
{
    T angle = parameterAngle(pos);
    return advcpp::ellipse::GetPointByParameterAngle(
        angle,
        m_center,
        m_majorDir,
        m_minorDir,
        m_majorRadius,
        m_minorRadius);
}

template<typename T>
advcpp::in4D::Vector<T>
Ellipse<T>::tangent(T pos)
{
    T angle = parameterAngle(pos);
    advcpp::in4D::Vector<T> tangent = advcpp::ellipse::GetTangentByParameterAngle(
        m_normal,
        m_majorDir,
        m_majorRadius,
        m_minorRadius,
        angle);
    return tangent;
}

} // namespace trajseg

#endif /* TRAJSEG_CPP_Ellipse_H_ */
