# Trajectory Segments C++

## Name
Full name: Trajectory Segments C++ library.

Short name: trajseg-cpp.

## Description
The project is aimed to provide trajectory primitives that can be used to manipulate trajectory.

## License
The project is licensed under the GNU Lesser General Public License, version 2.1 or later license.

## Status
Currently the project is just a test for the experience from potential employer (for the junior C++ developer vacation). The implementation differs from initial test requirements to be suitable for real life usage.
